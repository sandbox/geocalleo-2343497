SUMMARY
-------
This module extends the Phonetic Wordfilter module to deny content submission
instead of its default behaviour.

INSTALLATION
------------
Normal Drupal module installation:
http://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------
* Enable the module. Make sure the Phonetic Wordfilter is enabled as well.
* Use the admin configuration page (admin/config/content/phoneticfilter) to
  configure the node submission deny settings. You can find the settings for
  this module under "Deny Node Submission Settings".
* Check the box "Deny node submission" if you would like Phonetic Wordfilter to
  filter to deny node submission.
* Update the default error that will be shown to the user submitting the node
  letting them know to edit their content and remove the words in question.
